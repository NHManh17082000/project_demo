/*
    A friend class can access private and protected members of other classes in which 
    it is declared as a friend
    Declare A is a friend of B it is not meaning that B also is a friend of A( it is one-way relation ship)

*/
#include <iostream>

using namespace std;
class room
{
    private:
    int temp;
    public:
    room()
    {
        this->temp=23;
    }
    void set()
    {
        cout<<"enter the temp: "<<endl;
        cin>>this->temp;
    }
    friend class center;
};
class center
{
    private:
    int fan;
    public:
    center()
    {
        this->fan=0;
    }
    void fan_handle(room &a);
};
void center::fan_handle(room &a)
{
    if (a.temp > 20)
    {
        this->fan=1;
        cout<<"turn on fan"<<endl;
    }
    else
    {
        this->fan=0;
        cout<<"turn off fan"<<endl;
    }
}
int main()
{
    room living_room;
    center control;
    living_room.set();
    control.fan_handle(living_room);
}
