/*
    Multiple Inheritance is a feature of C++ where a class can inherit from more than one class. 
    i.e one subclass is inherited from more than one base class.
*/
#include <iostream>

using namespace std;
class A
{
    protected:
    int a;
    public:
    void get_A()
    {
        cout<<"enter the value of A "<<endl;
        cin>> a;
    }
    void display_A()
    {
        cout << "A = "<<a<<endl;
    }
};
class B 
{
    protected:
    int b;
    public:
    void get_B()
    {
        cout<<"enter the value of B "<<endl;
        cin>>b;
    }
    void display_B()
    {
        cout << "B = "<<b<<endl;
    }
};
class C:public A,public B
{
    public:
    void get_C()
    {
        get_B();
        get_A();
    }
    void display_C()
    {
        display_A();
        display_B();
    }
};
int main()
{
    C c;
    c.get_C();
    c.display_C();
}