/*
    there are some operators that cannot be overloaded. They are:

Scope resolution operator                                : :
Member selection operator                               
Member selection through                                   *                               
 
Pointer to member variable

Conditional operator                                         ? :
Sizeof operator                                             sizeof()
 
Operators that can be overloaded

Binary Arithmetic     ->     +, -, *, /, %
Unary Arithmetic     ->     +, -, ++, —
Assignment     ->     =, +=,*=, /=,-=, %=
Bit- wise      ->     & , | , << , >> , ~ , ^
De-referencing     ->     (->)
Dynamic memory allocation and De-allocation     ->     New, delete 
Subscript     ->     [ ]
Function call     ->     ()
Logical      ->     &,  | |, !
Relational     ->     >, < , = =, <=, >=
*/
#include <iostream>
using namespace std;
class complex_num
{
    private:
    int real;
    int imag;
    public:
    complex_num(int r=0,int i=0)
    {
        real=r;
        imag=i;
    }
    complex_num operator +(complex_num &object)
    {
        complex_num a;
        a.real=object.real+real;
        a.imag=imag+object.imag;
        return a;
    }
    void display()
    {
        cout<<this->real<<"+ i"<<this->imag<<endl;
    }
};
int main()
{
    complex_num r1(2,3),r2(3,4);
    complex_num r3=r1+r2;
    r3.display();
}