#include <iostream>
#include <string>
/*
    friend function can be: +) a global function
                            +) a function of another class
                            +) declare in any section of the class(public,private,protected)
*/
using namespace std;
class friend2;
class friend1
{
    private:
    string name1;
    int age1;
    public:
    friend1()
    {
        this->name1="";
        this->age1=0;
    }
    friend void get_in4(friend1 &x,friend2 &y);
    friend void display_all(friend1 x, friend2 y);
};
class friend2
{
    private:
    string name2;
    int age2;
    public:
    friend2()
    {
        this->name2="";
        this->age2=0;
    }
    friend void get_in4(friend1 &x,friend2 &y);
    friend void display_all(friend1 x,friend2 y);
};
/*
    be careful! when you use function, you need make sure that what is the parameter passing
*/
void get_in4(friend1 &x,friend2 &y)
{
    cout<<"enter the name of person 1"<<endl;
    getline(cin,x.name1);
    cout<<"enter the age of person 1"<<endl;
    cin>>x.age1;
    cin.ignore();
    cout<<"enter the name of person 2"<<endl;
    getline(cin,y.name2);
    cout<<"enter the age of person 2"<<endl;
    cin>>y.age2;

}
void display_all(friend1 x,friend2 y)
{
    cout<<"name 1:"<<x.name1<<"\n";
    cout<<"age  1:"<<x.age1<<"\n";
    cout<<"name 2:"<<y.name2<<"\n";
    cout<<"age  2:"<<y.age2<<"\n";
}
int main()
{
    friend1 x;
    friend2 y;
    get_in4(x,y);
    display_all(x,y);
    
}
