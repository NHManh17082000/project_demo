/*
    The capability of a class to derive properties and characteristics from another class is called Inheritance. 
    Inheritance is one of the most important features of Object-Oriented Programming. 

    When we say derived class inherits the base class, it means, the derived class inherits all the properties 
    of the base class, without changing the properties of base class and may add new features to its own. 
    These new features in the derived class will not affect the base class. The derived class is the 
    specialized class for the base class.

    +) Sub Class: The class that inherits properties from another class is called Subclass or Derived Class. 
    +) Super Class: The class whose properties are inherited by a subclass is called Base Class or Superclass

    Syntax: 

    class  <derived_class_name> : <access-specifier> <base_class_name>
    {
        //body
    }
    Where
    +) class      — keyword to create a new class
    +)derived_class_name   — name of the new class, which will inherit the base class
    +)access-specifier  — either of private, public or protected. If neither is specified, PRIVATE is taken as default
    +) base-class-name  — name of the base class
*/
#include <iostream>
#include <string>

using namespace std;

class person
{
    private:
        string name;
        int age;
    public:
    person()
    {
        this->name="";
        this->age=0;
    }
    void get_in4_p()
    {
        cout<<"enter the name: "<<"\n";
        getline(cin,this->name);
        //fflush(stdin);// xoa du lieu buffer de nhan su lieu moi - xem xet lai su dung ham cin.ignore();
        cout<<"enter the age: "<<"\n";
        cin>>this->age;
    }
    void display_p()
    {
        cout<<"name: "<<this->name<<"\n";
        cout<<"age : "<<this->age<<"\n";
    }
};
/*
    the member of a class is private by default and it is the difference between class and struct because of
    by default, the member of the struct is public
*/
class student:public person
{
    int MSSV;
    string major;
    public:
    void get_in4_s()
    {
        get_in4_p();
        cout<<"enter the Student ID: "<<"\n";
        cin>>this->MSSV;
        //fflush(stdin);
        cin.ignore();
        cout<<"enter the major: "<<"\n";
        getline(cin,this->major);
    }
    void display_s()
    {
        display_p();
        cout<<"Student ID: "<<this->MSSV<<"\n";
        cout<<"Major :"<<this->major<<"\n";
    }
};
int main()
{
    student A;
    A.get_in4_s();
    A.display_s();
}