/*
    Tạo class People gồm:

Thuộc tính: name, age,address để lưu lần lượt các giá trị tên, tuổi và địa chỉ.
Phương thức trong class People gồm: set(), get() là hàm nhập và xuất; hàm khởi tạo không tham số và hàm huỷ.
Tạo class Students kế thừa từ class People.

Class Students sẽ có thêm:

Thuộc tính id để lưu mã sinh viên, math lưu điểm môn toán, physical để lưu điểm môn vật lý, chemistry để lưu điểm môn hoá học.
Phương thức: set(), get(), GPA() để tính điểm trung bình 3 môn học
*/
#include <iostream>
#include <string>
using namespace std;

class people
{
    private:
    string name;
    int age;
    string address;
    public:
    void set_p()
    {
        cout<<"enter the name: "<<endl;
        getline(cin,name);
        cout<<"enter the age: "<<endl;
        cin>>age;
        cin.ignore();
        cout<<"enter the address: "<<endl;
        getline(cin,address);
    }
    void get_p()
    {
        cout<<"name: "<<name<<endl;
        cout<<"age : "<<age<<endl;
        cout<<"address:"<<address<<endl;
    }
};
class student:public people
{
    private:
    int id;
    int math;
    int physic;
    int chemistry;
    public:
    void set_s()
    {
        set_p();
        cout<<"enter student id: "<<endl;
        cin>>id;
        cout<<"enter the student's math: "<<endl;
        cin>>math;
        cout<<"enter the student's physic: "<<endl;
        cin>>physic;
        cout<<"enter the student's chemistry: "<<endl;
        cin>>chemistry;
    }
    void get_s()
    {
        get_p();
        cout<<"ID : "<<id<<"\n";
        cout<<" math:" <<math<<"\n";
        cout<<"chemistry:"<<chemistry<<"\n";
        cout<<"physic:"<<physic<<"\n";
    }
    void CPA()
    {
        cout<<"CPA :"<<double((math+physic+chemistry))/3<<endl;
    }

};
int main()
{
    student A;
    A.set_s();
    A.get_s();
    A.CPA();
}