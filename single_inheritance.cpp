/*
    In single inheritance, a class is allowed to inherit from only one class. 
    i.e. one subclass is inherited by one base class only
*/
#include <iostream>

using namespace std;
class A
{
    public: 
    void display()
    {
        cout<<"this is single inheritance example"<<endl;
    }
};
class B: public A
{

};
int main()
{
    B b;
    b.display();
}