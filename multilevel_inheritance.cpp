/*
    In this type of inheritance, a derived class is created from another derived class.
*/
#include <iostream>
using namespace std;

// Lớp cha
class Vehicle
{
public:
    Vehicle()
    {
        cout << "This is a Vehicle" << endl;
    }
};
// Lớp con kế thừa từ lớp cha
class fourWheeler : public Vehicle
{
public:
    fourWheeler()
    {
        cout << "Objects with 4 wheels are vehicles" << endl;
    }
};
// Lớp con kế thừa từ lớp cha thứ 2
class Car : public fourWheeler
{
public:
    car()
    {
        cout << "Car has 4 Wheels" << endl;
    }
};

// main function
int main()
{
    Car obj;
    return 0;
}