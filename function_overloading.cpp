/*
    +) When a function name is overloaded with different jobs it is called Function Overloading. 
    function overloaded need to have the same name and different parameter 
    +) This technique allows the same name to be used for “same” (same purpose) functions. 
    But different in parameter data type or number of parameters.
    +) The idea of the function overloading is when you use many function with different name to solve
     one problem but it has many case(many different parameter) -> we can use a all function with a 
     same name then when the compiler builde code it will choose the most siutable function. 

     How does Function Overloading work?
        Exact match:- (Function name and Parameter)
        If a not exact match is found:–
               ->Char, Unsigned char, and short are promoted to an int.

               ->Float is promoted to double

        If no match is found:
               ->C++ tries to find a match through the standard conversion.

        ELSE ERROR 
*/
#include <iostream>

using namespace std;

void sum(int a,int b)
{
    cout<<"sum of a and b is "<<(a+b)<<endl;
}
void sum(int a,int b,int c)
{
    cout<<"sum of a and b is "<<(a+b+c)<<endl;
}
void sum(double a, double b)
{
    cout<<"sum of a and b is "<<(a+b)<<endl;
}
int main()
{
    sum(1,2);
    sum(3.2,4.5);
    sum(1,2,3);
}
