#include <iostream>
#include <string>
using namespace std;

class student
{
    private:
    string name;
    int age;
    //int MSSV;
    string major;
    float CPA;
    public:
    student()
    {
        this->age=0;
        this->name ="";
        this->major="";
        this->CPA=0;
    }
    ~student()
    {
        this->age=0;
        this->name ="";
        this->major="";
        this->CPA=0;
    }
    void get_in4();
    void display_in4();
};
void student::get_in4()
{
    //cin.ignore();
    cout<<"enter the name: "<<"\n";
    getline(cin,this->name);
    cout<<"enter the age:"<<"\n";
    cin>>this->age;
    /*
        if we not use the cin.ignore() in the following line it can make some problem because of the following reason
        the funtion "cin" will leave a newline in the input buffer. so in the next operation it can be affected by that 
        and it will seem to skip the wait for return key and hence abnormality. to avoid that use cin.ignore()
    */
    cin.ignore();
    cout<<"enter the major:"<<"\n";
    getline(cin,this->major);
    cout<<"enter the CPA"<<"\n";
    cin>>this->CPA;
}
void student::display_in4()
{
    cout<<"name : "<<this->name<<"\n";
    cout<<"age  : "<<this->age<<"\n";
    cout<<"major: "<<this->major<<"\n";
    cout<<"CPA  : "<<this->CPA<<"\n";
}
int main()
{
    student HS1;
    cout<<"Before:"<<"\n";
    HS1.display_in4();
    cout<<"Display student information"<<"\n";
    HS1.get_in4();
    cout<<"after: "<<"\n";
    cout<<"Display student information"<<"\n";
    HS1.display_in4();
    
}